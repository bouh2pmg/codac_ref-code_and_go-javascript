  
$(function() {

    // CREATE
    $('#create').on("click", function() {
        var valtext = $('#textCreate:text').val();
        var valSelect = $('select').val();
        $('li').show();

        if (valtext === "") {
            $('h1').html("<span class='error'>Merci de renseigner le champs</span>");
        } else {
            if (valSelect === "email") {
                if (isEmail(valtext)) {
                    $('ul').append('<li class="' + valSelect + '">' + valtext + '</li>');
                } else {
                    $('h1').html("<span class='error'>Merci de renseigner un champs email valide</span>");
                }
            } else {
                $('ul').append('<li class="' + valSelect + '">' + valtext + '</li>');
            }
            
        }
    })

    // SEARCH
    $('#buttonSearch').on("click", function() {
        var valSearch = $("#search").val();
        var inputSearch = $('#inputSearch').val();

        // SELECT
        $('li').show();
        if (valSearch === "note") {
            $('li').not('.note').hide();
        } else if (valSearch === "email") {
            $('li').not('.email').hide();
        } else if (valSearch === "todo") {
            $('li').not('.todo').hide();
        }

        if (inputSearch !== "") {
            // WORD
            $("li").hide();
            if (valSearch === "note") {
                $("li:contains('" + inputSearch + "')").show();
                $('li').not('.note').hide();
            } else if (valSearch === "email") {
                $("li:contains('" + inputSearch + "')").show();
                $('li').not('.email').hide();
            } else if (valSearch === "todo") {
                $("li:contains('" + inputSearch + "')").show();
                $('li').not('.todo').hide();
            } else {
                $("li:contains('" + inputSearch + "')").show();
            }
        }
        
    })


});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(email)) {
       return false;
    } else{
       return true;
    }
}

 







