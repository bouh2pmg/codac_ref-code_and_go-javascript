  
$(function() {

    
    $(':submit').on("click", function() {
        var valtext = $(':text').val();
        var valSelect = $('select').val();

        if (valtext === "") {
            $('h1').html("<span class='error'>Merci de renseigner le champs</span>");
        } else {
            if (valSelect === "email") {
                if (isEmail(valtext)) {
                    $('ul').append('<li class="' + valSelect + '">' + valtext + '</li>');
                } else {
                    $('h1').html("<span class='error'>Merci de renseigner un champs email valide</span>");
                }
            } else {
                $('ul').append('<li class="' + valSelect + '">' + valtext + '</li>');
            }
            
        }
    })
    

});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
       return false;
    }else{
       return true;
    }
  }