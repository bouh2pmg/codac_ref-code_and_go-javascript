<?php
header('Content-type: application/json');

define ("DB_HOST","localhost");
define ("DB_USERNAME", "root");
define ("DB_PASSWORD", "mew");
define ("DB_PORT", "80");
define ("DB_NAME", "AJAX");

function connect_db($host, $username, $passwd, $port, $db)
{
	try {
		$pdo = new PDO("mysql:host=$host;port=$port;dbname=$db", $username, $passwd); 
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// echo("Connection to DB successful".PHP_EOL);
		return($pdo);
	}
	catch (PDOException $e) {
		echo "Error connection to DB".PHP_EOL;
	}
}
$pdo = connect_db(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_PORT, DB_NAME);

$req = $pdo->query("SELECT * FROM todo");
$reponse = $req->fetch();
//$data = $reponse.

echo json_encode($reponse);
