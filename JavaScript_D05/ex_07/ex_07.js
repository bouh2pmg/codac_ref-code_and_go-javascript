// SELECT TODO
$('#selectCreate').on("change", function() {
    var selectCreate = $(this).val();
    
    if (selectCreate === "todo") {
        $('<input type="text" id="tagCreate" placeholder="Entrer vos tags" />').insertAfter('#textCreate');
    } else {
        $('#tagCreate').remove();
    }
})

// CREATE LIST
$('#create').on("click", function() {
    var valtext = $('#textCreate:text').val();
    var valSelect = $('select').val();
    var todoVal = $('#tagCreate').val();

    //console.log(todoVal);
    //console.log($('span'));

    $('li').show();

    if (valtext === "") {
        $('h1').html("<span class='error'>Merci de renseigner le champs</span>");
    } else {
        if (valSelect === "email") {
            if (isEmail(valtext)) {
                $('ul').append('<li class="' + valSelect + '">' + valtext + '</li>');
            } else {
                $('h1').html("<span class='error'>Merci de renseigner un champs email valide</span>");
            }
        } if (valSelect === "todo") {
            var e = $('<li class="' + valSelect + '">' + valtext + '</li><span>' + todoVal + '</span><a href="#" class="addTag" style="color:blue;text-decoration: underline;cursor: pointer">Add Tag</a>');
            $('ul').append(e);
            // ADD TAG
            $('.addTag').on("click", function() {
                var el = $('<div class="add"><input type="text" placeholder="Entrer vos tags" /><button class="newTag">OK</button></div>');
                $(this).hide();
                $(this).parent().append(el);
                $('.newTag').on("click", function() {
                    var newTag = $(this).prev().val();
                    $('.addTag').show();
                    $(this).parent().html('<span>' + newTag + '</span>');
                });
            });
            
            
        } else {
            $('ul').append('<li class="' + valSelect + '">' + valtext + '</li>');
        }
        
    }
})

// SEARCH
$('#buttonSearch').on("click", function() {
    var valSearch = $("#search").val();
    var inputSearch = $('#inputSearch').val();

    // SELECT
    $('li').show();
    if (valSearch === "note") {
        $('li').not('.note').hide();
    } else if (valSearch === "email") {
        $('li').not('.email').hide();
    } else if (valSearch === "todo") {
        $('li').not('.todo').hide();
    }

    if (inputSearch !== "") {
        // WORD
        $("li").hide();
        if (valSearch === "note") {
            $("li:contains('" + inputSearch + "')").show();
            $('li').not('.note').hide();
        } else if (valSearch === "email") {
            $("li:contains('" + inputSearch + "')").show();
            $('li').not('.email').hide();
        } else if (valSearch === "todo") {
            $("li:contains('" + inputSearch + "')").show();
            $('li').not('.todo').hide();
        } else {
            $("li:contains('" + inputSearch + "')").show();
        }
    }
    
})

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(email)) {
       return false;
    } else{
       return true;
    }
}

// AJAX ADD
$("#buttonAjax").click(function(){
    
    $.ajax({
        url : 'server.php',
        type : 'GET',
        data: { get_param: 'value' }, 
        dataType : 'json',
        success : function(data, statut){
            //console.log(data);
            $.each(data, function(index, element) {
                var el = $('<li class="note">' + element + '</li>');
                $('ul').append(el);
            });
        },


        error : function(resultat, statut, erreur){
        
        },

        complete : function(resultat, statut){

        }
    });
   
});
