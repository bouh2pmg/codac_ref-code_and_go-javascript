$(document).ready(function(){

  class Player {
  	constructor(symbol) {	this.symbol = symbol;	}
  }

  function displayGrid(size) {
    $("td").css({
      border : '2px solid #ccc',
      width : '50px',
      height : '50px'
    });
  }

  function play(caseP, player) {
    switch (player.symbol) {
      case "X":
        caseP.css("background-color", "red");
        turnPlayer = 2;
        break;
      case "O":
        caseP.css("background-color", "blue");
        turnPlayer = 1;
        break;
    }
  }

  function checkFinished(grid) {
    // first: check if a player has won
    // lines
    for (var i = 0; i < 9; i+=3) {
      if (grid[i] != 0 && grid[i] == grid[i+1] && grid[i+1] == grid[i+2])
        return "Le joueur "+grid[i]+" a remporté la partie !";
    }
    // columns
    for (var i = 0; i < 3; i++) {
      if (grid[i] != 0 && grid[i] == grid[i+3] && grid[i+3] == grid[i+6])
        return "Le joueur "+grid[i]+" a remporté la partie !";
    }
    // diagonal to right
    for (var i = 0; i < 9; i++) {
      if (grid[i] != 0 && grid[i] == grid[i+4] && grid[i+4] == grid[i+8])
        return "Le joueur "+grid[i]+" a remporté la partie !";
    }
    // diagnol to left
    for (var i = 2; i < 7; i++) {
      if (grid[i] != 0 && grid[i] == grid[i+2] && grid[i+2] == grid[i+4])
        return "Le joueur "+grid[i]+" a remporté la partie !";
    }

    // second: check if at least one case is empty
    for (var i = 0; i < 9; i++) {
      if (grid[i] == 0)
        return false;
    }
    return true;
  }

  var end = false;
  // var size = 9;
  var turnPlayer = 1;

  // create two players
  var player1 = new Player("X");
  var player2 = new Player("O");

  // create the grid
  var grid = [];
  for (var i = 0; i < 9; i++) {
    grid.push("0");
  }
  displayGrid();

  // play
  $("td").click(function() {
    var currentCase = $(this).attr('id');
    console.log(currentCase);
    // case is filled only if it is empty and the game is not over
    if (end == false && grid[currentCase] == 0) {
      if (turnPlayer == 1) {
        play($(this), player1);
        grid[currentCase] = 1;
        turnPlayer = 2;
      }
      else if (turnPlayer == 2) {
        play($(this), player2);
        grid[currentCase] = 2;
        turnPlayer = 1;
      }
      end = checkFinished(grid);
    }
    if (end != false) {
      if (end == true)
        alert("Egalité !");
      else
        alert(end);
    }
  });
});
