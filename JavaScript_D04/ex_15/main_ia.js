$(document).ready(function(){

  class Player {
  	constructor(number, ia) {
      this.number = number;
      this.ia = ia;
    }
  }

  function displayGrid(size) {
    $("h1").css({
      'text-align' : 'center',
      'font-family' : 'arial'
    });
    $("table").css({
      'border-collapse' : 'collapse',
      width : '60%',
      margin : '0 auto',
      position: 'relative'
    });
    $("td").css({
      border : '5px solid #ffdd7e',
      width : '33%',
      'padding-top' : '33%',
      height : 'auto',
      position: 'relative'
    });
  }

  function play(grid, caseP, player) {
    console.log("case = "+caseP.attr("id")+' player = '+player);
    switch (player) {
      case 1:
        caseP.css("background-color", "#e95280");
        checkFinished(grid);
        turnPlayer = 2;
        break;
      case 2:
        caseP.css("background-color", "#23b1a5");
        checkFinished(grid);
        turnPlayer = 1;
        break;
    }
  }

  function checkFinished(grid) {
    // first: check if a player has won
    // lines
    console.log(grid[0]+grid[1]+grid[2]+grid[3]+grid[4]+grid[5]+grid[6]+grid[7]+grid[8]);
    for (var i = 0; i < 9; i+=3) {
      if (grid[i] != 0 && grid[i] == grid[i+1] && grid[i+1] == grid[i+2]) {
        return "Le joueur "+grid[i]+" a remporté la partie !";
      }
    }
    // columns
    for (var i = 0; i < 3; i++) {
      if (grid[i] != 0 && grid[i] == grid[i+3] && grid[i+3] == grid[i+6]){
        return "Le joueur "+grid[i]+" a remporté la partie !";
      }
    }
    // diagonal to right
    if (grid[4] != 0 && grid[0] == grid[4] && grid[4] == grid[8]){
      return "Le joueur "+grid[4]+" a remporté la partie !";
    }
    // diagnol to left
    if (grid[4] != 0 && grid[2] == grid[4] && grid[4] == grid[6]){
      return "Le joueur "+grid[4]+" a remporté la partie !";
    }

    // second: check if at least one case is empty
    for (var i = 0; i < 9; i++) {
      if (grid[i] == 0)
        return false;
    }
    return "Egalité !";
  }

  function playIA(grid, player) {
    // adds the last case if two are already filled in a row

    for (var i = 0; i < 9; i+=3) { // lines
      if (grid[i] == player && grid[i+1] == player && grid[i+2] == 0) {
        play(grid, $("td#"+(i+2)), player);
        return i+2;
      }
      else if (grid[i+1] == player && grid[i+2] == player && grid[i] == 0) {
        play(grid, $("td#"+i), player);
        return i;
      }
      else if (grid[i] == player && grid[i+2] == player && grid[i+1] == 0) {
        play(grid, $("td#"+(i+1)), player);
        return i+1;
      }
    }
    for (var i = 0; i < 3; i++) { // columns
      if (grid[i] == player && grid[i+3] == player && grid[i+6] == 0) {
        play(grid, $("td#"+(i+6)), player);
        return i+6;
      }
      else if (grid[i+3] == player && grid[i+6] == player && grid[i] == 0) {
        play(grid, $("td#"+i), player);
        return i;
      }
      else if (grid[i] == player && grid[i+6] == player && grid[i+3] == 0) {
        play(grid, $("td#"+(i+3)), player);
        return i+3;
      }
    }
    if (grid[0] == grid[4] && grid[0] == player && grid[8] == 0){ // diagonal to right
      play(grid, $("td#8"), player);
      return 8;
    }
    else if (grid[8] == grid[4] && grid[8] == player && grid[0] == 0){
      play(grid, $("td#0"), player);
      return 0;
    }
    else if (grid[8] == grid[0] && grid[8] == player && grid[4] == 0){
      play(grid, $("td#4"), player);
      return 4;
    }
    if (grid[2] == grid[4] && grid[2] == player && grid[6] == 0){ // diagonal to left
      play(grid, $("td#6"), player);
      return 6;
    }
    else if (grid[6] == grid[4] && grid[6] == player && grid[2] == 0){
      play(grid, $("td#2"), player);
      return 2;
    }
    else if (grid[6] == grid[2] && grid[6] == player && grid[4] == 0){
      play(grid, $("td#4"), player);
      return 4;
    }
    // blocks the way of the opponent if the latter has two in a row
    for (var i = 0; i < 9; i+=3) {
      if (grid[i] == grid[i+1] && grid[i] != 0 && grid[i] != player && grid[i+2] == 0) {
        play(grid, $("td#"+(i+2)), player);
        return i+2;
      }
      else if (grid[i+2] == grid[i+1] && grid[i+1] != 0 && grid[i+1] != player && grid[i] == 0) {
        play(grid, $("td#"+i), player);
        return i;
      }
      else if (grid[i] == grid[i+2] && grid[i] != 0 && grid[i] != player && grid[i+1] == 0) {
        play(grid, $("td#"+(i+1)), player);
        return i+1;
      }
    }
    for (var i = 0; i < 3; i++) { // columns
      if (grid[i] == grid[i+3] && grid[i] != 0 && grid[i] != player && grid[i+6] == 0) {
        play(grid, $("td#"+(i+6)), player);
        return i+6;
      }
      else if (grid[i+6] == grid[i+3] && grid[i+6] != 0 && grid[i+6] != player && grid[i] == 0) {
        play(grid, $("td#"+i), player);
        return i;
      }
      else if (grid[i] == grid[i+6] && grid[i] != 0 && grid[i] != player && grid[i+3] == 0) {
        play(grid, $("td#"+(i+3)), player);
        return i+3;
      }
    }

    // first turn (if AI is player1) -> play corner
    if (player == 1) {
      for (var i = 0; i < 9; i+=6) {
        if (grid[i] == 0) {
          play(grid, $("td#"+i), player);
          return i;
        }
        else if (grid[i+2] == 0) {
          play(grid, $("td#"+i), player);
          return i;
        }
      }
    }
    // AI = PLAYER 2

    if (player == 2) {
    // after first turn
      for (var i = 0; i < 3; i+=2) { // player1 played corner -> play edge
        if (i == 0 && grid[i] == 1 && grid[i+8] == 1) {
          for (var j = 1; j < 8; j+=2) {
            if (grid[j] == 0) {
              play(grid, $("td#"+j), player);
              return j;
            }
          }
        }
        else if (i == 2 && grid[i] == 1 && grid[i+4] == 1) {
          for (var j = 1; j < 8; j+=2) {
            if (grid[j] == 0) {
              play(grid, $("td#"+j), player);
              return j;
            }
          }
        }
      }

      // first turn
      for (var i = 0; i < 9; i+=6) { // player1 played corner -> play center
        if (grid[i] == 1 && grid[4] == 0) {
          play(grid, $("td#4"), player);
          return 4;
        }
        else if (grid[i+2] == 1 && grid[4] == 0) {
          play(grid, $("td#4"), player);
          return 4;
        }
      }
      for (var i = 1; i < 8; i+=2) { // player1 played edge -> center or one of the edges
        if (grid[i] == 1) {
          if (grid[4] == 0) {
            play(grid, $("td#4"), player);
            return 4;
          }
          else if ((i == 1 || i == 7) && grid[i] == 1 && grid[i-1] == 0) {
            play(grid, $("td#"+(i-1)), player);
            return i-1;
          }
          else if ((i == 1 || i == 7) && grid[i] == 1 && grid[i+1] == 0) {
            play(grid, $("td#"+(i+1)), player);
            return i+1;
          }
          else if ((i == 3 || i == 5) && grid[i] == 1 && grid[i-1] == 0) {
            play(grid, $("td#"+(i-1)), player);
            return i-1;
          }
          else if ((i == 3 || i == 5) && grid[i] == 1 && grid[i+1] == 0) {
            play(grid, $("td#"+(i+1)), player);
            return i+1;
          }
        }
      }
      for (var i = 0; i < 9; i+=2) { // player1 played center -> play corner
        if (grid[4] == 1) {
          if (grid[i] == 0) {
            play(grid, $("td#"+i), player);
            return i;
          }
        }
      }
    }

    // last possibilities
    for (var i = 0; i < 9; i+=2) { // play remaining empty corner
      if (grid[i] == 0) {
        play(grid, $("td#"+i), player);
        return i;
      }
    }
    for (var i = 1; i < 8; i+=2) { // play remaining empty edge
      if (grid[i] == 0) {
        play(grid, $("td#"+i), player);
        return i;
      }
    }

  }

  var end = false;
  var turnPlayer = 1;

  // create two players
  var player1 = new Player(1, false);
  // var player2 = new Player(2, false);
  var player2 = new Player(2, true);

  // create the grid
  var grid = [];
  for (var i = 0; i < 9; i++) {
    grid.push("0");
  }
  displayGrid();

  // play
  $("td").click(function() {
    var currentCase = $(this).attr('id');
    // console.log(currentCase);
    // case is filled only if it is empty and the game is not over
    if (end == false && grid[currentCase] == 0) {
      if (turnPlayer == 1 && player1.ia == false) {
        play(grid, $(this), 1);
        grid[currentCase] = 1;
        if (player2.ia == true) {
          currentCase = playIA(grid, 2);
          grid[currentCase] = 2;
          turnPlayer = 1;
        }
        else {
          turnPlayer = 2;
        }
      }
      else if (turnPlayer == 2 && player2.ia == false) {
        play(grid, $(this), 2);
        grid[currentCase] = 2;
        if (player1.ia == true) {
          currentCase = playIA(grid, 1);
          grid[currentCase] = 1;
          turnPlayer = 2;
        }
        else {
          turnPlayer = 1;
        }
      }
      end = checkFinished(grid);
    }
    if (end != false) {
        alert(end);
    }
  });
});
