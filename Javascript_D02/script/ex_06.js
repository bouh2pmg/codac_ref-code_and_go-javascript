window.addEventListener("load", function(){
    var divElt = document.querySelector("footer div");

    class Hero 
    {
        constructor(nom, type, intelligence, strength)
        {
            this.nom = nom;
            this.type = type;
            this.intelligence = intelligence;
            this.strength = strength;
        }

        toString()
        {
            var strengthP = "point";
            var intelP = "point";
            if(this.strength > 1) strengthP = "points";
            if(this.intelligence > 1) intelP = "points";

            divElt.innerHTML += "I am " + this.nom + " the " + this.type + ",I have " + this.intelligence + " intelligence "+ intelP +" and " + this.strength + " strength "+ strengthP +"<br>";
        }
    }

    var mage = new Hero("amadeus", "mage", 10, 3);
    var warrior = new Hero("pontius", "warrior", 1, 8);
    mage.toString();
    warrior.toString();
});