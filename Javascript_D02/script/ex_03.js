window.addEventListener("load", function(){
    
    var divElt = document.querySelector("footer div");

    divElt.addEventListener("click", function(){

        var name = "";

        while(name === "")
        {
            name = prompt("What's your name ?");
            name = name.trim();
        }

        if(confirm("Are you sure that " + name + " is your name ?"))
        {
            divElt.textContent = "Hello " + name + " !";
            alert("Hello " + name + " !");
        }
    });

});