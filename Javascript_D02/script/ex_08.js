window.addEventListener("load", function(){
    var divCanvasElts = document.querySelector("footer div");
    var canvasElts = document.querySelectorAll("footer div canvas");

    var tempCanvas = [];

    for(let i=0; i<canvasElts.length; i++)
    {
        if(getComputedStyle(canvasElts[i]).getPropertyValue("background-color") === "rgb(255, 165, 0)")
        {
            canvasElts[i].style.backgroundColor = getComputedStyle(canvasElts[i]).getPropertyValue("background-color");
            tempCanvas.push(canvasElts[i]);
        }
    }

    for(let i=0; i<canvasElts.length; i++)
    {
        if(getComputedStyle(canvasElts[i]).getPropertyValue("background-color") === "rgb(128, 0, 128)")
        {
            canvasElts[i].style.backgroundColor = getComputedStyle(canvasElts[i]).getPropertyValue("background-color");
            tempCanvas.push(canvasElts[i]);
        }
    }

    for(let i=0; i<canvasElts.length; i++)
    {
        if(getComputedStyle(canvasElts[i]).getPropertyValue("background-color") === "rgb(0, 0, 0)")
        {
            canvasElts[i].style.backgroundColor = getComputedStyle(canvasElts[i]).getPropertyValue("background-color");
            tempCanvas.push(canvasElts[i]);
        }
    }

    for(let i=0; i<canvasElts.length; i++)
    {
        if(getComputedStyle(canvasElts[i]).getPropertyValue("background-color") === "rgb(128, 128, 0)")
        {
            canvasElts[i].style.backgroundColor = getComputedStyle(canvasElts[i]).getPropertyValue("background-color");
            tempCanvas.push(canvasElts[i]);
        }
    }

    divCanvasElts.innerHTML = "";

    tempCanvas.forEach(function(canvas){
        divCanvasElts.appendChild(canvas);
    });

});