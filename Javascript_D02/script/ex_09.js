//Code provient grandement d'OpenClassRoom

window.addEventListener("load", function(){
    
    var canvasDiv = document.querySelector("canvas");
    var footerDiv = document.querySelector("footer div");
    canvasDiv.style.position = "absolute";

    var storage = {}; // Contient l'objet de la div en cours de déplacement

    canvasDiv.addEventListener('mousedown', function(e) { // Initialise le drag & drop
            var s = storage;
            s.target = e.target;
            s.offsetX = e.clientX - s.target.offsetLeft;
            s.offsetY = e.clientY - s.target.offsetTop;
        });

    canvasDiv.addEventListener('mouseup', function() { // Termine le drag & drop
        storage = {};
    });

    document.addEventListener('mousemove', function(e) { // Permet le suivi du drag & drop
        var target = storage.target;

        if (target) {
            target.style.top = e.clientY - storage.offsetY + 'px';
            target.style.left = e.clientX - storage.offsetX + 'px';
        }

    });




});