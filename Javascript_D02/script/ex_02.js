window.addEventListener("load", function(){

    var compteurClick = 0;

    var divElt = document.querySelector("footer div");

    divElt.addEventListener("click", function(){
        compteurClick++;
        if(compteurClick < 2)
        {
            divElt.textContent = compteurClick + " click";
        }
        else
        {
            divElt.textContent = compteurClick + " clicks";
        }
        
    });

});