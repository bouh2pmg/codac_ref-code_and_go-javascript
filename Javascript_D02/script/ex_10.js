window.addEventListener("load", function(){
    
    var divElt = document.querySelector("footer div");
    //Creation+insertion box delete
    var deleteDiv = divElt.cloneNode(true);
    deleteDiv.style.display = "none";
    deleteDiv.innerHTML = "<button>Delete cookies</button>";
    document.querySelector("footer").appendChild(deleteDiv);

    //Ajout du cookie au click + ajout de la box delete
    divElt.querySelector("a").addEventListener("click", function(e){
        document.cookie = "acceptsCookies=true; max-age=86400";
        deleteDiv.style.display ="block";
        divElt.style.display ="none";
    })

    //suppression cookie via button
    deleteDiv.querySelector("button").addEventListener("click", function(e){
        document.cookie = "acceptsCookies=; max-age=0";
        deleteDiv.style.display ="none";
        divElt.style.display ="block";
    })

    //actions verification du cookie
    if (document.cookie.split(';').filter((item) => item.includes('acceptsCookies=true')).length) {
        deleteDiv.style.display ="block";
        divElt.style.display ="none";
    }

});