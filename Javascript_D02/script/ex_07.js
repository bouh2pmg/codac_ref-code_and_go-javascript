window.addEventListener("load", function(){

    //Musique -----------------------------------------------------------------
        //Creation + insertion de la balise audio
    var audioElt = document.createElement("audio");
    audioElt.src = "https://www.youtube.com/audiolibrary_download?f=m&vid=87e54780b34fe0a5";
    document.getElementsByTagName("footer")[0].lastElementChild.appendChild(audioElt);

            //Gestion clicks sur boutons
    audioElt = document.getElementsByTagName("audio")[0];
    document.querySelectorAll("footer div button")[0].textContent = "Play";
                //Play/Pause
    document.querySelectorAll("footer div button")[0].addEventListener("click", function(e){
        if(audioElt.paused)
        {
            audioElt.play();
            e.target.textContent = "Pause";
        }
        else
        {
            audioElt.pause();
            e.target.textContent = "Play";
        }
    });
            //Stop
    document.querySelectorAll("footer div button")[1].addEventListener("click", function(e){
 
            audioElt.pause();
            audioElt.currentTime = 0;
            e.target.previousElementSibling.textContent = "Play";
    });

            //Mute
    document.querySelectorAll("footer div button")[2].addEventListener("click", function(e){

        if(audioElt.volume > 0)
        {
            audioElt.volume = 0;
            e.target.textContent = "Unmute";
        }
        else
        {
            audioElt.volume = 1;
            e.target.textContent = "mute";
        }
    });
    //Canvas -----------------------------------------------------------------
    
    var canvasElt = document.querySelector("canvas");
    //canvasElt.width = 100;
    //canvasElt.height = 100;

    var ctx = canvasElt.getContext("2d");
    ctx.fillStyle = "white";
    ctx.strokeStyle = "white";
    ctx.lineWidth = 1;

    ctx.beginPath();
    ctx.moveTo(6,6)
    ctx.lineTo(14,10);
    ctx.lineTo(6,14);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();

});