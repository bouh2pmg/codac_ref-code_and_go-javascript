window.addEventListener("load", function(){
    
    var buttonElts = document.querySelectorAll("footer div button");
    var selectElt = document.querySelector("footer div select");

    buttonElts.forEach(function(button){
        button.addEventListener("click", function(e){
            if(e.target.textContent.trim() === "+")
            {
               document.body.style.fontSize = (parseInt(getComputedStyle(document.body).fontSize) + 1) + "px";
            }
            else if (e.target.textContent.trim() === "-")
            {
                document.body.style.fontSize = (parseInt(getComputedStyle(document.body).fontSize) - 1) + "px";
            }
        });
    });



        selectElt.addEventListener("change", function(e) {
            var color = e.target.value;
            if(color.trim() !== "")
            {
                document.body.style.backgroundColor = color;
            }
        });

});