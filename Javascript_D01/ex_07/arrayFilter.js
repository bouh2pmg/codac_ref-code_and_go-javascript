// Your implementation
function arrayFilter(input, filtre)
{
    if(Array.isArray(input))
    {
        var id=0;
        var result;
        var retour=[];

        while(id<=input.length)
        {
            //console.log("test");
            result =filtre(input[id]);
            if(result==true)
            {
                retour.push(input[id]);
            }
            id++;
        }
        return retour;
    }
    else
    {
    console.log("Pas un tableau");
    return;
    }


}

// Use this to test
var toFilter = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// the anonymous function is the test your filtering function will use to make a decision
var passed = arrayFilter(toFilter, function (value) {
return value % 2 === 0;
});
console.log(passed);